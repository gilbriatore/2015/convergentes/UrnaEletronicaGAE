package br.edu.up;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/cadastro")
public class Cadastro {
    @GET
    @Path("/xml")
    @Produces("application/xml")
    public Candidato getCandidatoXML() {
        return new Candidato("Hello World!", 42);
    }
    
    @GET
    @Path("/json")
    @Produces("application/json")
    public Candidato getCandidatoJSON() {
        return new Candidato("Hello World!", 42);
    }
 
    @POST
    @Consumes("application/xml")
    public String postMyBean(Candidato myBean) {
        return myBean.nome;
    }
}