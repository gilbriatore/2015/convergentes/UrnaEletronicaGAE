package br.edu.up;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Candidato {
    @XmlElement
    public String nome;
    @XmlElement
    public int idade;
 
    public Candidato(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }
 
    // empty constructor needed for deserialization by JAXB
    public Candidato() {
    }
 
    @Override
    public String toString() {
        return "Candidato{" +
            "nome='" + nome + '\'' +
            ", idade=" + idade +
            '}';
    }
}